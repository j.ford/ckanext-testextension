const colourWheel = [
    '#cc33ff',
    '#6699ff',
    '#ff0066',
    '#ffcc66',
    '#33cc33',
    '#e085c2',
    '#e62e00',
];


const splitColourWheel = [
    '#cc33ff',
	  '#000000',
    '#9900cc',
    '#6699ff',
	  '#000000',
    '#3377ff',
    '#ff0066',
	  '#000000',
    '#b30047',
    '#ffcc66',
	  '#000000',
    '#ffb31a',
    '#33cc33',
	  '#000000',
    '#248f24',
    '#e085c2',
	  '#000000',
    '#cc3399',
    '#e62e00',
	  '#000000',
    '#cc2900',
];

(function (ckan, jQuery) {
  /**
   * Draws a chord diagram within an HTML div. Uses D3 library.
   *
   * @param: div         The HTML div name where the graph is to appear.
   * @param: matrix      The data matrix to fill the graph.
   * @param: names	   The city names for labels.
   * @param: split_flag  True if the chord is to show cities split between incoming and outgoing.
   */
  ckan.chordDiagram = function (div, 
                                matrix, 
                                names, 
                                split_flag) {

    var margin = {left: 100, top: 100, right: 100, bottom:100},
      width = Math.min(window.innerWidth, 1000) - margin.left - margin.right,
      height = Math.min(window.innerWidth, 1000) - margin.top - margin.bottom,
      innerRadius = Math.min(width, height) * .39,
      outerRadius = innerRadius * 1.1,
      opacityDefault = 0.7,
      spacingBetweenGroups = (split_flag)? .01: .04,
      labelOmissionCap = 130,
      fadeOutDegree = .05;

    if (split_flag) {
      colours = splitColourWheel
    } else {
      colours = colourWheel
    }


    var colours = d3.scale.ordinal()
      .domain(d3.range((split_flag)? names.length * 3: names.length))
      .range(colours);

    var chord = d3.layout.chord()
      .padding(spacingBetweenGroups)
      .sortChords(d3.descending)
      .matrix(matrix);

    var arc = d3.svg.arc()
      .innerRadius(innerRadius * 1.01)
      .outerRadius(outerRadius);

    var path = d3.svg.chord()
      .radius(innerRadius);


    var svg = d3.select(div).append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" +
        (width / 2 + margin.left) + "," + (height / 2 + margin.top) + ")");


    function getGradID(d) {
      return "linkGrad-" + d.source.index + "-" + d.target.index;
    }

    var grads = svg.append("defs").selectAll("linearGradient")
      .data(chord.chords())
      .enter().append("linearGradient")
      //Create the unique ID for this specific source-target pairing
      .attr("id", getGradID)
      .attr("gradientUnits", "userSpaceOnUse")
      //Find the location where the source chord starts
      .attr("x1", function (d) {
        return innerRadius * Math.cos(
          (d.source.endAngle - d.source.startAngle) / 2 + d.source.startAngle - Math.PI / 2
        );
      })
      .attr("y1", function (d) {
        return innerRadius * Math.sin(
          (d.source.endAngle - d.source.startAngle) / 2 + d.source.startAngle - Math.PI / 2
        );
      })
      //Find the location where the target chord starts
      .attr("x2", function (d) {
        return innerRadius * Math.cos(
          (d.target.endAngle - d.target.startAngle) / 2 + d.target.startAngle - Math.PI / 2
        );
      })
      .attr("y2", function (d) {
        return innerRadius * Math.sin(
          (d.target.endAngle - d.target.startAngle) / 2 + d.target.startAngle - Math.PI / 2
        );
      });

    grads.append("stop")
      .attr("offset", "0%")
      .attr("stop-color", function (d) {
        return colours(d.source.index);
      });

    grads.append("stop")
      .attr("offset", "100%")
      .attr("stop-color", function (d) {
        return colours(d.target.index);
      });


    var outerArcs = svg.selectAll("g.group")
      .data(chord.groups)
      .enter().append("g")
      .attr("class", "group")
      .on("mouseover", fade(fadeOutDegree))
      .on("mouseout", fade(opacityDefault));

    outerArcs.append("path")
      .style("fill", function (d) {
        return colours(d.index);
      })
      .style("stroke", function (d) {
              return '#404040'
          })
      .attr("id", function(d) { return "group" + d.index; })
      .attr("d", arc);

    if (split_flag) {
      outerArcs.append("text")
        .attr("x", 6)
        .attr("dy", 15)
        .append("textPath")
        .attr("xlink:href", function (d) {
          return "#group" + d.index;
        })
        .text(function (d, i) {
          if (i % 3 == 0) {
            if (d.value > labelOmissionCap * 1.5) {
              return "IN";
            } else if (d.value > labelOmissionCap)
              return "I";
          } else if ((i-2) % 3 == 0 && d.value > labelOmissionCap) {
            if (d.value > labelOmissionCap * 2) {
              return "OUT";
            } else if (d.value > labelOmissionCap)
              return "O";
          }
        })
        .style("fill", "white");
    }


    outerArcs.append("text")
      .each(function (d) {
        d.angle = (d.startAngle + d.endAngle) / 2;
      })
      .attr("dy", ".35em")
      .attr("class", "titles")
      .attr("text-anchor", function (d) {
        return d.angle > Math.PI ? "end" : null;
      })
      .attr("transform", function (d) {
        return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")"
          + "translate(" + (outerRadius + 10) + ")"
          + (d.angle > Math.PI ? "rotate(180)" : "");
      })
      .text(function (d, i) {
        if (split_flag) {
          if ((i-1) % 3 == 0) {
            return names[Math.floor(i/3)];
          } else {
            return null;
          }
        } else {
          return names[i];
        }
      });


    svg.selectAll("path.chord")
      .data(chord.chords)
      .enter().append("path")
      .attr("class", "chord")
      //change the fill to reference the unique gradient ID of the source-target combination
      .style("fill", function (d) {
        return "url(#" + getGradID(d) + ")";
      })
      .style("stroke", function (d) {
              return '#404040'
          })
      .style("opacity", opacityDefault)
      .attr("d", path);

    
    function fade(opacity) {
      return function (d, i) {
        svg.selectAll("path.chord")
          .filter(function (d) {
            if ((i-1) % 3 == 0 && split_flag){
              return !(
                (d.source.index == i || d.target.index == i)
                || (d.source.index == i+1 || d.target.index == i+1)
                || (d.source.index == i-1 || d.target.index == i-1)
              )
            } else {
              return !(d.source.index == i || d.target.index == i)
            }
          })
          .transition()
          .style("opacity", opacity);
      }
    }
    return svg;
  }
})(this.ckan, this.jQuery)
