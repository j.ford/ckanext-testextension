from six import text_type

import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit


def show_most_popular_groups():
    """Return the value of the most_popular_groups config setting.
    
    To enable showing the most popular groups, add this line to the
    [app:main] section of your CKAN config file::

        ckan.testextension.most_popular_groups = true

    Returns ``False`` by default, if the setting is not in the config file.

    :rtype: bool
    """
    value = config.get('ckan.testextension_most_popular_groups', False)
    value = toolkit.asbool(value)
    return value


def group_create(context):
    """Restricts group creation to members of the curators and admins."""
    # Get the user name of the logged-in user.
    user_name = context['user']

    # Get a list of the members of the 'curators' group.
    try:
        members = toolkit.get_action('member_list')(
            data_dict={'id': 'curators', 'object_type': 'user'})
    except toolkit.ObjectNotFound:
        # The curators group doesn't exist
        return {'success': False,
                'msg': "The curators have not been assembled."}

    # 'members' is a list of (user_id, object_type, capacity) tuples, we're
    # only interested in the user_ids
    member_ids = [member_tuple[0] for member_tuple in members]

    # We have the logged-in user's user name, get their used id.
    convert_user_name_or_id_to_id = toolkit.get_converter(
        'convert_user_name_or_id_to_id')
    try:
        user_id = convert_user_name_or_id_to_id(user_name, context)
    except toolkit.Invalid:
        # The user doesn't exist (e.g. they're not logged in).
        return {'success': False,
                'msg': "You must be logged in as a member of the curators"
                       "group to create new groups."}

    # Finally, we can test whether the user is a member of the curators group.
    if user_id in member_ids:
        return {'success': True}

    return {'success': False,
            'msg': "Only curators are allowed to create groups."}


def most_popular_groups():
    '''Return a sorted list of the groups with the most datasets.'''

    groups = toolkit.get_action('group_list')(
        data_dict={'sort': 'package_count desc' ,'all_fields': True})

    return groups[:10]


class TestextensionPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IAuthFunctions)
    plugins.implements(plugins.ITemplateHelpers)

    # IConfigurer
    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'testextension')

    def update_config_schema(self, schema):
        ignore_missing = toolkit.get_validator('ignore_missing')
        is_positive_integer = toolkit.get_validator('is_positive_integer')
        schema.update({
            'ckan.datasets_per_page': [ignore_missing,
                                       is_positive_integer],
            'ckanext.testextension.test_conf': [ignore_missing,
                                                text_type],
        })
        return schema

    # IAuthFunctions 
    def get_auth_functions(self):
        return {'group_create': group_create}

    # ITemplateHelpers
    def get_helpers(self):
        '''
        Register the most_popular_groups() function above as a templates
        helper function.
        '''
        return {
            'testextension_most_popular_groups': most_popular_groups,
            'testextension_show_most_popular_groups': show_most_popular_groups
        }

